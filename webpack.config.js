var path = require('path')
var LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = function(env, config, options){

    return {
        entry: {
            'app':'./src/index.js',
        },
        output: {
            path: './build',
            filename: 'app.js',
            chunkFilename: '[name].js',
            publicPath: './build/',
        },
        performance: {
          hints: false
        },
        devtool:'eval-source-map',
        module: {
            loaders: [
                { test: /\.js$/, exclude: /(node_modules|bower_components)/, loader: 'babel-loader', query:{compact: false}},
            ]
        },
        plugins:[
            // new LiveReloadPlugin({
            //     port: 4000 + (Math.round(50*Math.random())),
            //     appendScriptTag:true
            // })
        ],
        resolve: {
            alias: {
                'src': path.resolve(process.cwd(), './src'),
            },
        },
    }

}







